# VOXL OpenCV 3.4.6

## Table of Contents

- [Summary](#summary)
- [Examples](#examples)
- [Install](#install)
- [Build](#build)
    - [Build 32-bit](#build-32-bit)
    - [Build 64-bit](#build-64-bit)


## Summary

This project contains examples that show how you can use OpenCV 3.4.6 on the VOXL.  It essentially takes what OpenCV has provided and packages it up for easier setup on the VOXL.

It contains some simple source code examples and an IPK that can be used to install OpenCV 3.4.6 on the VOXL to utlize the examples.

The project can also be used to build the OpenCV 3.4.6 IPK package, which utilizes the VOXL Emulator Docker Image, available for download at <https://developer.modalai.com>

## Examples

### Prerequisites

Host Computer

- Android Debug Bridge (adb)
- Python

Target

- the OpenCV 3.4.6 IPK must be installed (this is handled by a `deploy` script described in next section)
- ArUco contributor library for opencv necessary for apriltags example

### Examples Setup

The `examples` subdirectory contains some source code examples from OpenCV.

To use:

- connect the VOXL target to the host computer
- run the following command to install the OpenCV 3.4.6 IPK and examples

```bash
cd voxl-opencv-3-4-6/examples
# push and install the ipk and examples
python deploy.py
```

### Face Detect

Run the following commands to access the target and build an example face detection application as a simple test ("hot dog/not hot dog" coming soon...)

```bash
# connect to the target
adb shell

# build the example
cd /home/root/opencv/examples/face_detect
cmake .
make

# use it
./FaceDetect parrot.jpg
./FaceDetect person.jpg
```

### April Tag Detection

Run the following commands to access the target and build an example application to detect apriltags

```bash
# connect to the target
adb shell

# build the example
cd /home/root/opencv/examples/april_tag_detect
cmake .
make

# use it
./TagDetect -i -ci=<Insert camera ID here>
./TagDetect -i -f=<Insert path to image/video file here>
./TagDetect -h
```

### Live Camera Feed
An example using OpenCV with a live camera feed can be found [here](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellovideo)

## Install

A prebuilt 32-bit IPK is available for download [here](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96.v7-a.neon.vfpv4.ipk).

This is deployed to the target using the `examples/deploy.py` script.

EXPERIMENTAL: A 64-bit IPK is available for download [here](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96_aarch64.ipk).

### Note
When developing on the VOXL Emulator or on target, it may be necessary uninstall OpenCV 2.4 first. These are the commands to remove the old OpenCV 2.4:

```
opkg remove lib32-opencv --force-removal-of-dependent-packages
opkg remove lib32-libopencv --force-removal-of-dependent-packages
opkg remove lib32-libopencv-core2.4 --force-removal-of-dependent-packages
opkg remove lib32-opencv-dbg --force-removal-of-dependent-packages
```

## Build 32-bit

The following explains how to Build OpenCV and package as an IPK.

1. The `voxl-emulator` docker image is used to build. Please follow the voxl-docker instructions here:
https://gitlab.com/voxl-public/voxl-docker

2. On the host system, clone this repo and update the submodules to get the OpenCV source:

```bash
# clone this repo if you haven't already
git clone https://gitlab.com/voxl-public/voxl-opencv-3-4-6.git
cd voxl-opencv-3-4-6

# and get opencv 3.4.6
git submodule update --init --recursive
cd packge/opencv
git checkout 33b765d7979fd8a6038026aa44f6ff1a9c082b7b

# return to the project's /package directory
cd ..
```

Now, we will build using the `voxl-emulator`.

```bash
# run the voxl-emulator docker from the voxl-opencv-3-4-6/package directory
voxl-docker -i voxl-emualtor

# build opencv and install into ipk/data for packaging
./build.sh

# and make the IPK
./make_package.sh

# exit from the voxl-emulator
exit
```

The IPK will be available in the `voxl-opencv-3-4-6/package` directory.

### Run Tests

On host:

```bash
adb shell "mkdir -p /home/root/opencv/bin"
adb push opencv/build/bin /home/root/opencv/bin
```

On target:

```bash
adb shell
cd /home/root/opencv/bin/
./opencv_test_core
```

### CMake Options

Interesting options to try on VOXL platform:

```bash
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=$PREFIX/ipk/data/usr -D WITH_OPENMP=ON ..
```

## Build 64-bit

--- EXPERIMENTAL ---

The following explains how to Build OpenCV and package as an IPK for use with aarch64 or 64-bit applications. A 64-bit IPK is available for download [here](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96_aarch64.ipk).

1. The `voxl-cross64` docker image is used to build.
```
$ wget https://gitlab.com/voxl-public/voxl-docker/raw/master/voxl-cross64/Dockerfile
$ docker build -t voxl-cross64 .
```


2. On the host system, clone this repo and update the submodules to get the OpenCV source:

```
# clone this repo if you haven't already
$ git clone https://gitlab.com/voxl-public/voxl-opencv-3-4-6.git
$ cd voxl-opencv-3-4-6

# and get opencv 3.4.6
$ git submodule update --init --recursive
$ cd packge/opencv
$ git checkout 33b765d7979fd8a6038026aa44f6ff1a9c082b7b

# return to the project's /package directory
$ cd ..

# Add support for 64-bit toolchain and GCC-4.9
$ wget https://gitlab.com/voxl-public/voxl-docker/raw/master/voxl-cross64/aarch64-gnu-4.9.toolchain.cmake
$ mv aarch64-gnu-4.9.toolchain.cmake opencv/platforms/linux/
```

NOTE: For OpenCV 4.2.0 replace *git checkout 33b765d7979fd8a6038026aa44f6ff1a9c082b7b* with **git checkout tags/4.2.0**

Now, we will build using the `voxl-cross64`.


```
$ docker run -v `pwd`:/opt/data/workspace/ -it voxl-cross64
$ cd /opt/data/workspace/

# build opencv and install into ipk/data for packaging
$ ./clean.sh
$ ./build64.sh

# and make the IPK
$ ./make_package64.sh

# exit from the voxl-emulator
$ exit
```
### Build Example

```
# OpenCV 3.4.6
cmake -DOpenCV_DIR=/opt/data/workspace/ipk/data/usr/share/OpenCV/ -DCMAKE_TOOLCHAIN_FILE=/opt/data/workspace/opencv/platforms/linux/aarch64-gnu-4.9.toolchain.cmake -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" ../

# OpenCV 4.2.0
cmake -DOpenCV_DIR=/opt/data/workspace/ipk/data/usr/lib64/cmake/opencv4/ -DCMAKE_TOOLCHAIN_FILE=/opt/data/workspace/opencv/platforms/linux/aarch64-gnu-4.9.toolchain.cmake -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" ../
```

#### Run Example
```
./FaceDetect4.2.0 --cascade=/usr/share/opencv4/haarcascades/haarcascade_frontalface_alt.xml --nested-cascade=/usr/share/opencv4/haarcascades/haarcascade_eye_tree_eyeglasses.xml person.jpg
```

### Build 64-bit static

Add -DBUILD_SHARED_LIBS=OFF as so:
```cmake -DCMAKE_TOOLCHAIN_FILE=/opt/data/workspace/opencv/platforms/linux/aarch64-gnu-4.9.toolchain.cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=OFF -DWITH_OPENCL=OFF -DHAVE_OPENCL=0 -DCMAKE_INSTALL_PREFIX=/opt/data/workspace/ipk/data/usr -DENABLE_NEON=ON -DWITH_OPENMP=ON -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" ..```


### (OLD) Build 64-bit directly from opencv.git

* Use Dockerfile found [here](https://gitlab.com/voxl-public/voxl-docker/tree/master/voxl-cross64)
```
$ docker build -t voxl-cross64 .
$ ./run_docker.sh`
$ https://github.com/opencv/opencv.git
$ git clone https://github.com/opencv/opencv.git
$ cp aarch64-gnu-4.9.toolchain.cmake opencv/platforms/linux
$ cd opencv
$ git checkout 33b765d7979fd8a6038026aa44f6ff1a9c082b7b
$ mkdir build
$ cd build
$ cmake -DCMAKE_TOOLCHAIN_FILE=/opt/data/workspace/opencv/platforms/linux/aarch64-gnu-4.9.toolchain.cmake -DCMAKE_BUILD_TYPE=RELEASE -DWITH_OPENCL=OFF -DHAVE_OPENCL=0 -DCMAKE_INSTALL_PREFIX=/opt/data/workspace/ipk/data/usr -DENABLE_NEON=ON -DWITH_OPENMP=ON -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" ..
$ make -j7
$ make install

```

CMake command for cross-compiling an OpenCV example or other app:
```
cmake -DOpenCV_DIR=/opt/data/workspace/ipk/data/usr/share/OpenCV/ -DCMAKE_TOOLCHAIN_FILE=/opt/data/workspace/opencv/platforms/linux/aarch64-gnu-4.9.toolchain.cmake ../
```





# New Instructions for 4.5.1 and voxl-cross

```
git submodule init
git submodule update

# start docker
voxl-docker -i voxl-cross
```

inside docker:
```
./build.sh
./make_package.sh
```

